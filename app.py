from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy

from sqlalchemy import Table, Column, Integer, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

app = Flask(__name__)
app.debug = True

app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql+psycopg2://postgres:Hola123@localhost/colegio'

db = SQLAlchemy(app)

materias_estudiante = db.Table('materias_estudiante', db.Column('materias_id', db.Integer, db.ForeignKey('materias.id'), primary_key=True),
db.Column('estudiante_id', db.Integer, db.ForeignKey('estudiante.id'), primary_key=True),
)

class maestros(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	nombre = db.Column(db.String(20))
	edad = db.Column(db.Integer, nullable=True)

	def __repr__(self):
		return f'{self.nombre}'


class materias(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	nombre = db.Column(db.String(60))
	impartidor = db.Column(db.String(60))

	def __repr__(self):
                return f'{self.nombre}'

class estudiante(db.Model):
	id = db.Column(db.Integer, primary_key=True) 
	nombre = db.Column(db.String(30))
	edad = db.Column(db.Integer)
	maestro_id = db.Column(db.Integer, db.ForeignKey('maestros.id'))
	maestro = db.relationship('maestro', backref=db.backref('estudiantes', lazy=True))
	materias = db.relationship('materias', secondary=materias_estudiante,
			backref=db.backref('estudiantes', lazy=True))
	def __repr__(self):
                return f'{self.nombre}'





if __name__ == '__main__':
        app.run()
